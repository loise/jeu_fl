note
	description: "Classe joueur {JOUEUR} sert a ccree un joueur qui est contoler par l'utilisateur."
	author: "Jonathan Parent"
	date: "2020-05-19"


class
	JOUEUR

inherit
	PERSONAGE
		rename
			make as make_personage
		redefine
			have_colide
		end

create
	make

feature {NONE} --Constructure
	make(a_renderer: GAME_RENDERER; a_nom_photo: STRING_32)
			--Sert a cree un object de la classe `PERSONAGE'
		do
			make_personage (a_renderer, a_nom_photo)
			going_right := false
			going_left := false
			going_up := false
			going_down := false
			vecteur_de_vitesse := 1
			can_move_object:=true
			level:=0
			set_cycle_to_move (0)
		end

feature
	sprint
			--Lorsque on appelle `sprint' on modifie la vitesse
		do
			vecteur_de_vitesse := 2
		end

	stop_sprint
			--Lorsque on appelle `stop_sprint' on modifie la vitesse
		do
			vecteur_de_vitesse := 1
		end

	have_colide
			--Ce qui se passe l'orseque `Current' entre en colision
		do

		end

feature	--Acces

	level:INTEGER_32 assign set_level
		--Level de `Current' sert a changer de map

	set_level(a_level:INTEGER_32)
		-- Assign la valeur de `level' avec `a_level'

		do
			level := a_level
		ensure
			Is_Assign: level = a_level
		end


feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end


