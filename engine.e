note
	description: "Engin de jeut, pour ger�e la mecanique du jeu {ENGINE}."
	author: "Jonathan Parent"
	date: "2020-04-21"


class
	ENGINE

inherit
	GAME_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED

--	THREAD
--		rename
--			make as make_thread
--		end

create
	make




feature {NONE} -- Initialisation
	make
		-- Initialisation de `Current'
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER

		do
			--make_thread
			--has_error := false
			create l_window_builder
			l_window_builder.set_dimension (1500, 700)
			l_window_builder.set_title ("jeu somnabule")
			l_window_builder.enable_must_renderer_synchronize_update	-- Ask to the video card to manage the frame synchronisation (FPS)
			window := l_window_builder.generate_window
			--info des texture

			create map.make (window.renderer, 0)
			create red.make (255, 0, 0,0)
			create yellow.make (255,255,0,0)
			create black_color.make_rgb (250, 0, 0)
			create player.make (window.renderer, "orange.jpg")
			create liste_copy.make

			set_liste_object(map.liste_object)

			win:=true
			create serveur.make_client
			serveur.somnambule := map.somnabule
			map.joueur.level :=0

			create fleche_factory.make (window.renderer)


			--has_error := desert.has_error
		end


feature -- Attributs

	fleche_factory:FACTORY_FLECHE_DIRECTION
			--Sert a cree la factory qui va afficher les fleche

	has_error:BOOLEAN
			--Regarder si il y a une erreur

	window:GAME_WINDOW_RENDERED
			-- The window to draw the scene

	red: GAME_COLOR
			--Couleur rouge

	yellow:GAME_COLOR
			--Couleur jaune

	black_color:GAME_COLOR
			--Couleur noire

	win:BOOLEAN
			--Boolean pour la gestion de gagner

	map:MAP
			--Objec map pour cr�e la map

	serveur:SERVEUR
			--Object `SERVEUR' pour gerer les conection



	meuble_selectioner: detachable MEUBLE_DEPLACABLE
			--Object selectioner par le joueur

	liste_object: LIST [OBJECT]
			-- `liste_object'









feature

	run
		--Sert a lancer le jeu
		require
			No_Error: not has_error

		do


			window.key_pressed_actions.extend (agent on_key_press)
			window.key_released_actions.extend (agent on_key_release)
			game_library.iteration_actions.extend (agent on_iteration)
			game_library.quit_signal_actions.extend (agent on_quit) --A FAIRE


			if window.renderer.driver.is_present_synchronized_supported then	-- If the Video card accepted the frame synchronisation (FPS)
				game_library.launch_no_delay									-- Don't let the library managed the frame synchronisation
			else
				game_library.launch
			end
		end



feature {NONE} --implementation


	on_iteration(a_timestamp:NATURAL_32)

		do
			window.renderer.clear
			window.renderer.draw_texture (map.background, 0,0)
			window.renderer.draw_texture (map.hud, 0,0)
			audio_library.update

			change_level (a_timestamp)

			player.update (a_timestamp)
			map.somnabule.update (a_timestamp)

			if has_win(map.lit,map.somnabule)then
				player.level :=	player.level+1
				win:=true
			end
			if map.somnabule.reveil_point =0 then--pour restar la map
				win:=true
				change_level(a_timestamp)
			end


			item_tester_colision (player,a_timestamp, void)
			item_tester_colision (map.somnabule,a_timestamp, void)
			item_tester_colision(map.chien,a_timestamp,void)

			afficher_object (liste_object, a_timestamp)		---afficher la liste
			afficher_fleche
			window.renderer.draw_texture (player, player.x, player.y)
			window.renderer.draw_texture (map.somnabule, map.somnabule.x, map.somnabule.y)

			desiner_bar_de_someil
			window.renderer.present
		end

	has_win(a_lit:LIT;a_sommanbul:SOMNAMBULE):BOOLEAN
		do
			if a_sommanbul.has_colide (a_lit,void) then
				Result := true
			end
		end

	change_level(a_timestamp:NATURAL_32)
		do
			if win then
				serveur.write_serveur ("WINER")
				player := map.joueur

				if player.level= 1 then
					map.map0 (a_timestamp)

				elseif player.level= 2 then
					map.map1 (a_timestamp)

				elseif player.level= 3 then
					map.map2 (a_timestamp)

				end
				if player.level= 4 then

					--player.level:=0
					win:=true
				end
				liste_copy.wipe_out
				liste_copy.append (liste_object)
				liste_copy.extend (map.somnabule)
				set_liste_object (map.liste_object)
				map.somnabule.reveil_point := 10

				win := false
			end
		end

	afficher_fleche
			--Sert a aficher la fleche qui dit dans quelle direction le meuble va renvoyer le somnabule
		local
			l_fleche:OBJECT_INERTE
		do
			from
				liste_copy.start
			until
				liste_copy.after
			loop
				if attached {MEUBLE_DEPLACABLE} liste_copy.item as meuble then
					if meuble.send_up then
						l_fleche:= fleche_factory.fleche_up ( meuble.x, meuble.y)
					end
					if meuble.send_left then
						l_fleche:= fleche_factory.fleche_left ( meuble.x, meuble.y)
					end
					if meuble.send_right then
						l_fleche:= fleche_factory.fleche_right ( meuble.x, meuble.y)
					end
					if meuble.send_down then
						l_fleche:= fleche_factory.fleche_down ( meuble.x, meuble.y)
					end
					if meuble.send_back then
						l_fleche:= fleche_factory.fleche_all_side ( meuble.x, meuble.y)
					end
					if attached l_fleche then
						window.renderer.draw_texture (l_fleche, l_fleche.x, l_fleche.y)
					end
				end
				liste_copy.forth
			end
		end



	item_tester_colision(a_personage:PERSONAGE;a_time: NATURAL_32;a_meuble_colision: detachable OBJECT)
			--Sert a tester tous les colision que `a_personage' peut avoire rencontre dans `liste_copy'
		local
			l_colision:BOOLEAN
			l_cursor:LINKED_LIST_ITERATION_CURSOR[ OBJECT]
		do
			from
				l_cursor := liste_copy.new_cursor
				l_cursor.start
			until
				l_cursor.after
			loop
				if attached {OBJECT} l_cursor.item as object  then
					--gestion_rebort_map(object)
					if attached {MEUBLE} l_cursor.item as current_object  then
						--if attached {COLLISABLE} a_meuble_colision as meuble_colision  then
							l_colision:=current_object.has_colide (a_personage,void)
							--gestion_rebort_map(current_object)
							if l_colision then
								if l_colision then
									if a_personage.can_move_object  then
										if current_object.is_movable  then
											calcule_le_mouvement (current_object,a_personage)
											l_colision:=item_tester_colision_individuel(current_object,a_personage,false,a_personage)
											--current_object.deplacement := player.deplacement

										else
											a_personage.anuler_mouvement (a_personage)
										end
									else
										cant_move_object(a_personage,a_time,current_object)
									end
								end
							end
						--end
					end
				end
				l_cursor.forth
			end
		end

	cant_move_object(a_personage:PERSONAGE;a_time:NATURAL_32;a_current_object:MEUBLE)
			--Gestion des fonction a faire l'orsque  `a_personage' entre en colison
		do
			a_personage.anuler_mouvement (a_personage)
			a_personage.effect
			a_personage.have_colide
			has_colide (a_personage,a_current_object, a_time)
			--a_personage.inverse_mouvement
		end




	item_tester_colision_individuel(a_object,a_personage:COLLISABLE;a_colision:BOOLEAN ;a_precedent_colision: detachable COLLISABLE):BOOLEAN
			--Gestion des colision avec une recursive et gestion des deplacment
		local
			--l_colision:BOOLEAN
			l_cursor:LINKED_LIST_ITERATION_CURSOR[ OBJECT]
			l_return:BOOLEAN
		do
			l_return:=a_colision

			from
				l_cursor := liste_copy.new_cursor
				l_cursor.start
			until
				l_cursor.after
			loop

				if attached {OBJECT} l_cursor.item as object  then
					if attached {COLLISABLE}l_cursor.item as current_object then
						if attached {COLLISABLE}a_precedent_colision as precedent_colision then
							if a_object/=current_object and current_object/=precedent_colision then
								if current_object.has_colide (a_object,precedent_colision) then
									if current_object.is_movable then
										--gestion_rebort_map(current_object)
										calcule_le_mouvement (current_object,a_personage )
										l_return:=item_tester_colision_individuel(current_object,a_personage,false,a_object)
										---------Gestion accepteable
										if not l_return then
											if current_object.has_colide (a_object,precedent_colision) then
												a_personage.anuler_mouvement (a_personage)
											end
											precedent_colision.anuler_mouvement (a_personage)
										else
											calcule_le_mouvement (current_object,a_personage )
										end
									else
										a_object.anuler_mouvement (a_personage)
										precedent_colision.anuler_mouvement (a_personage)
									end
								end
							end
						end
					end
				end
				l_cursor.forth
			end
			Result :=l_return
		end




	tourner_un_meuble(a_liste_object:LIST [OBJECT])
			--Faire tourner le meuble le plus proche
		local
			l_i:INTEGER_32

			l_valeur_x:INTEGER_32

			l_have_rotate:BOOLEAN
			l_point_central_x:INTEGER_32
			l_point_central_y:INTEGER_32
		do
			l_have_rotate:=false

			l_point_central_x := (player.x + player.get_x2)//2
			l_point_central_y := (player.y + player.get_y2)//2

			from
				l_i:=50
			until
				l_i=70 or l_have_rotate
			loop
				from
					a_liste_object.start
				until
					a_liste_object.off or l_have_rotate
				loop
					if attached {MEUBLE_DEPLACABLE} a_liste_object.item as meuble then	--chercher avec rayon
						if (l_point_central_x - meuble.x) <= l_i or (l_point_central_x - meuble.get_x2) <= l_i then
							if (l_point_central_y - meuble.y) <= l_i or (l_point_central_y - meuble.get_y2) <= l_i then
								meuble.rotate
								l_have_rotate:=true
							end
						end
						l_valeur_x := player.x +l_i
					end
				a_liste_object.forth
				end
				l_i:= l_i +1
			end
		end




	afficher_object(a_liste_object:LIST [OBJECT];a_timestamp:NATURAL_32)
			--Pour aficher les objet de la `a_liste_object'
		do
			from
				a_liste_object.start
			until
				a_liste_object.off
			loop


				if attached {ANIMAL} a_liste_object.item as animal then
					animal.patrole_a_to_b (a_timestamp)
				end

				if attached {OBJECT} a_liste_object.item as les_object then
					window.renderer.draw_texture (les_object,les_object.x,les_object.y)
				end

				a_liste_object.forth
			end
		end




	has_colide(a_personage:PERSONAGE;a_meuble:MEUBLE;a_time: NATURAL_32)
			--ce qui arive lorseque un personge autre le joeur entre en colision
		do
			if a_meuble.send_back then
				a_personage.inverse_mouvement
			elseif a_meuble.send_down then
				a_personage.stop_all
				a_personage.go_down (a_time)
			elseif a_meuble.send_up then
				a_personage.stop_all
				a_personage.go_up (a_time)
			elseif a_meuble.send_right then
				a_personage.stop_all
				a_personage.go_right (a_time)
			elseif a_meuble.send_left then
				a_personage.stop_all
				a_personage.go_left (a_time)
			end
		end


	desiner_bar_de_someil
			--Permet de calcule le niveut de la bar de someil et de l'afichier
		local		-----------------------------------------------------FAIRE POSTE CONTRA ET PRECONTRA
			l_valeur_de_la_bar:INTEGER_32	--Valeur de la bar en px
		do
			if  map.somnabule.reveil_point /= 0  then
				l_valeur_de_la_bar:= (10-map.somnabule.reveil_point)*17
				window.renderer.set_drawing_color (yellow)
				window.renderer.draw_filled_rectangle (1323, 32+l_valeur_de_la_bar, 41, 176-l_valeur_de_la_bar)
			else
				print("vous aver reveiler le somnanbule ")
				window.renderer.draw_texture (map.you_lose_scren, 0, 0)
			end
		end


	calcule_le_mouvement(a_object,a_object_mouve:COLLISABLE)
				--Donne `a_object' un deplacement selon la `direction' de `a_object_mouve'
		do
			if  a_object_mouve.going_right then 		--go_right
				a_object.x := a_object.x + a_object_mouve.deplacement
			end
			if  a_object_mouve.going_left then 		--go_left
				a_object.x := a_object.x - a_object_mouve.deplacement
			end
			if a_object_mouve.going_up then 			--go_up
				a_object.y := a_object.y - a_object_mouve.deplacement
			end
			if  a_object_mouve.going_down  then 	--go_down
				a_object.y := a_object.y + a_object_mouve.deplacement
			end

		end

	--------------------------------------------------------------------
	on_key_press(a_time:NATURAL_32;a_touche_presse:GAME_KEY_EVENT)
				--tester les touche qui sont presser
		do
			if not a_touche_presse.is_repeat then
				if a_touche_presse.is_d then
						player.go_right(a_time)

				end
				if a_touche_presse.is_a then
						player.go_left(a_time)

							--Test connection
						serveur.send_at_serveur (map.somnabule.reveil_point.out)


				end
				if a_touche_presse.is_s then
						player.go_down(a_time)


				end
				if a_touche_presse.is_w then
						player.go_up(a_time)
				end

				if a_touche_presse.is_e then
					tourner_un_meuble (liste_object)
				end
				if a_touche_presse.is_left_shift then
					player.sprint
				end
			end

		end


	on_key_release(a_time:NATURAL_32;a_touche_release:GAME_KEY_EVENT)
				--Lorsque une touche est relacher
		do
			if not a_touche_release.is_repeat then
				if a_touche_release.is_d then
					player.stop_right

				end
				if a_touche_release.is_a then
					player.stop_left
				end
				if a_touche_release.is_s then
					player.stop_down
				end
				if a_touche_release.is_w then
					player.stop_up
				end
				if a_touche_release.is_left_shift then
					player.stop_sprint
				end
			end
		end


	selectioner_meuble
			--lorseque e est presser regarde si il a un mauble selectioner
		do
			if attached meuble_selectioner as l_meuble then
				if l_meuble.is_selected then
					l_meuble.deselect

					--liste_copy.wipe_out
					--liste_copy.append (liste_object)
				else
					replacer_le_meuble_dans_la_liste

				end
			else
				replacer_le_meuble_dans_la_liste
			end
		end




	replacer_le_meuble_dans_la_liste
				--Lorsque on appuye sur e on on va lenser un teste sur `liste_object' pour voire si le joueur est pres du meuble

		do
			from					--	allore ajute le meuble au `meuble_selectioner'
				liste_object.start
			until
				liste_object.off
			loop
				if attached {MEUBLE_DEPLACABLE} liste_object.item as l_meuble then
					if l_meuble.can_move (player) then
						l_meuble.stop_all
						l_meuble.selecte

						meuble_selectioner :=  l_meuble
						l_meuble.deplacement := player.deplacement
					end
				end
				liste_object.forth
			end
		end


	on_quit(a_timestamp:NATURAL)
			-- Sert a fermer la fenetre
		do
			--l_socket.close
			game_library.stop
		end


feature --Acces


	set_liste_object(a_liste_object:ARRAYED_LIST [OBJECT])
			--Sert a cr�e la liste d' `OBJECT'
		do
			liste_object := a_liste_object

		end



	liste_copy:LINKED_LIST[ OBJECT]
			--est une copit de la liste de `OBJECT'


	player:JOUEUR assign set_player
		-- assigne le joueur `Current'

	set_player(a_player:JOUEUR)
			-- assigne le joueur value of `player' with `a_player'
		do
			player := a_player
		ensure
			Is_Assign: player = a_player
		end

feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"
end



