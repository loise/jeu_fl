note
	description: "Sert a cree une comunication entre le client et le serveur avec la classe {SERVEUR}."
	author: "Joanthan Parent"
	date: "2020-05-06"


class
	SERVEUR


create
	make_client,
	make_serveur

feature  -- Initialisation


	make_client
			-- Ex�cution du programme client
		local
			l_worker:WORKER_THREAD
		do
			create fichier.make

			arreter_thread:=false
			ip_reseau:=fichier.ip
			player_name:= fichier.player_name

			create socket.make_targeted (fichier.ip, fichier.port)
			create l_worker.make (agent boucle_thread_to_send)

			l_worker.launch
		end

	make_serveur
			-- Ex�cution du programme serveur
		do
			create fichier.make
			--make_thread
			ip_reseau:=fichier.ip
			player_name:="serveur"
			create socket.make_bound (fichier.port)
			read_and_write(socket, io.output)
		end


feature	--Fonctional


	write_serveur(a_text:STRING_32)
			--Envoyer une string au serveur
		do
			read_and_write_client(a_text,socket)
		end

	start_comunication
			--Sert a envoyer le caracter pour dire que le client commence a parler au serveur
		do
			socket.put_character ('[')
		end

	end_of_comunication
			--Sert a envoyer le caracter pour dire que le client a fini de parler au serveur
		do
			socket.put_character (']')
		end



	read_and_write_client(a_medium_in:STRING_32;a_medium_out:IO_MEDIUM)
			-- Transfert tous les caract�res entr�s dans `a_medium_in'
			-- dans la sortie `a_medium_out'.
			-- quite a la fin de la string `a_medium_in'
		local
			l_fin:BOOLEAN
			i:INTEGER_32
		do
			i:=1
			from
				l_fin:=false
			until
				l_fin
			loop
				a_medium_out.put_character (a_medium_in.at (i).to_character_8)

				if (i = a_medium_in.count) then

					l_fin:=true
				end
				i:= i +1
			end
			--a_medium_out.put_character ('/')
		end





	read_and_write(a_medium_in,a_medium_out:IO_MEDIUM)
			-- Transfert tous les caract�res entr�s dans `a_medium_in'
			-- dans la sortie `a_medium_out'.
			-- (Utilisez ` pour quitter).
		local
			l_end_comunicationt:BOOLEAN
			l_fin:BOOLEAN
			l_texte:STRING_32
			l_can_read:BOOLEAN
		do
			from
				l_end_comunicationt:=false
			until
				l_end_comunicationt
			loop
				l_texte:=""
				l_can_read:=false
				from
					l_fin:=false
				until
					l_fin
				loop
					a_medium_in.read_character
					if a_medium_in.last_character.is_equal ('[') and l_can_read=false then
						l_can_read:=true
					elseif a_medium_in.last_character.is_equal (']') and l_can_read=true then
						l_fin:=true
						l_can_read:=false

					elseif l_can_read then
						l_texte.append_character (a_medium_in.last_character)
					end
				end

				l_texte.trim
				if l_texte.count >3  then
					save_result (l_texte)
				end

				print("{"+l_texte+"}")

			end
		end

	boucle_thread_to_send
			--Sert a garder en memoire les element que le thread devra utiliser
			--Et les envoi aau serveur
		local
			l_old_point_de_reveil:INTEGER_32
		do
			l_old_point_de_reveil:=0
			from
			until
				arreter_thread
			loop
				if attached somnambule as l_somnambule then
					if  l_somnambule.reveil_point /= l_old_point_de_reveil  then
						l_old_point_de_reveil :=l_somnambule.reveil_point
						send_at_serveur("("+fichier.player_name+")<"+l_somnambule.reveil_point.out+">,")
					end

				end
				{EXECUTION_ENVIRONMENT}.sleep (10000)
			end
		end


	send_at_serveur(a_string:STRING_32)
		do
			start_comunication
			write_serveur (a_string)
			end_of_comunication
		end


	save_result(a_text:STRING_32)
			--Utilise `a_text' pour sauvegarder l'information
		do
			fichier.save (fichier.get_save_list,fichier.get_string_from_setting (a_text))
		end


	lancer_thread
			--Lance le thread
		do

		end


feature --Attributs

	fichier:FICHIER


	arreter_thread:BOOLEAN
			--Sert a savoir si le thread doi etre active ou pas


	socket: NETWORK_DATAGRAM_SOCKET
			--Socket sert a la connection

	ip_reseau:STRING_32 assign set_ip_reseau
			--Adresse de ip de connection `ip_reseau' de `Current'

	player_name:STRING_32
			--Nom du joeur selon le fichier seting

	set_ip_reseau(a_ip_reseau:STRING_32)
			--Assigne
		do
			ip_reseau := a_ip_reseau
		ensure
			Is_Assign: ip_reseau = a_ip_reseau
		end



	temps_ecouler:INTEGER_32
			--Le temp ecouler dans le niveaut actuel

	somnambule: detachable SOMNAMBULE assign set_somnambule
			--Ser pointer le somnabule pour la clase `SERVEUR'

	set_somnambule(a_somnambule:detachable SOMNAMBULE)
			--Assigner le somnabule
		do
			somnambule:= a_somnambule
		ensure
			Is_Assign: somnambule= a_somnambule
		end



feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end



