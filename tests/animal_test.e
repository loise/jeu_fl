note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "Jonathan Parent"
	date: "2020-05-27"
	testing: "type/manual"

class
	ANIMAL_TEST

inherit
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end


feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			audio_library.enable_playback
			game_library.enable_video -- Enable the video functionalities
			image_file_library.enable_image (true, false, false)
			create l_window_builder
			l_window_builder.set_dimension (1500, 700)
			window := l_window_builder.generate_window
			--create l_jeu.make
			create chien.make (window.renderer, "joueur.jpg","moli_qui_jap.wav")

		end

	on_clean
			-- Cette m�thode est lanc� apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do

			text_library.quit_library
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	set_point_test
			-- Test pour valider que les point son bien valider lors du `set_point_a' et `set_point_b'
		note
			testing:  "covers/{ANIMAL}.set_point_a","covers/{ANIMAL}.set_point_b", "execution/isolated", "execution/serial"
		do


			chien.set_point_a ([20,25])
			chien.set_point_b ([50,25])
			assert("{ANIMAL}.`set_point_a' change {ANIMAL}.`point_a' x = 20.", chien.point_a.at (1) = 20 )
			assert("{ANIMAL}.`set_point_a' change {ANIMAL}.`point_a' y = 25.", chien.point_a.at (2) = 25 )

			assert("{ANIMAL}.`set_point_b' change {ANIMAL}.`point_a' x = 50.", chien.point_b.at (1) = 50 )
			assert("{ANIMAL}.`set_point_b' change {ANIMAL}.`point_a' y = 25.", chien.point_b.at (2) = 25 )



		end


	patrole_a_to_b_test
			-- Test pour savoire si `patrole_a_to_b_test' Change bien la position de x
		note
			testing:  "covers/{ANIMAL}.patrole_a_to_b", "execution/isolated", "execution/serial"

		do
			chien.set_x (20)
			chien.set_y (25)
			chien.set_point_a ([20,25])
			chien.set_point_b ([50,25])
			chien.update (0)

			chien.patrole_a_to_b (10)
			chien.patrole_a_to_b (30)

			assert("{ANIMAL}.`set_point_a' change {ANIMAL}.`point_a' x >= 20.", chien.x >= 21 )
			chien.patrole_a_to_b (50)
			assert("{ANIMAL}.`set_point_a' change {ANIMAL}.`point_a' x = 50.", chien.x /= 20 )

		end

		patrole_a_to_b_x_negatif_test
			-- Test pour savoire si `patrole_a_to_b_test' Change bien la position de x
		note
			testing:  "covers/{ANIMAL}.patrole_a_to_b", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
			chien.set_x (20)
			chien.set_y (25)
			chien.set_point_a ([-20,25])
			assert("{ANIMAL}.`set_point_a' erron� non g�rer", false )
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end
	patrole_a_to_b_y_negatif_test
			-- Test pour savoire si `patrole_a_to_b_test' Change bien la position de x
		note
			testing:  "covers/{ANIMAL}.patrole_a_to_b", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
			chien.set_x (20)
			chien.set_y (25)
			chien.set_point_a ([20,-25])
			assert("{ANIMAL}.`set_point_a' erron� non g�rer", false )
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end



	set_positions_normal_test
		note
			testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"
		do
			chien.set_positions (100, 100)
			assert("{SOMNAMBULE}.`x' est positife.", chien.x = 100)
			assert("{SOMNAMBULE}.`y' est positife.", chien.y = 100)

		end
	set_positions_erroner_test
		note
			testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"

		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				chien.set_positions (-100, -100)
				assert("{SOMNAMBULE}.`set_positions'le x et y ne devrais pas autoriser set_positions(-100,-100) ",false)

			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	set_positions_limite_test
		note
			testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"
		do
			chien.set_positions (0, 0)
			assert("{SOMNAMBULE}.`set_positions'le x et y devrais autoriser set_positions(0,0) ",chien.x= 0)
			assert("{SOMNAMBULE}.`set_positions'le x et y devrais autoriser set_positions(0,0) ",chien.y= 0)
		end


feature {NONE}




	window:GAME_WINDOW_RENDERED
			-- La {GAME_WINDOW} utilis� pour g�n�rer `chien'

	chien:ANIMAL
			--L'objet tester dans `Current'



feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"



end


