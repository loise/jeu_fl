note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "Jonathan Parent"
	date: "2020-05-27"
	testing: "type/manual"

class
	NEW_TEST_PERSONAGE

inherit
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			audio_library.enable_playback
			game_library.enable_video -- Enable the video functionalities
			image_file_library.enable_image (true, false, false)
			create l_window_builder
			l_window_builder.set_dimension (1500, 700)
			window := l_window_builder.generate_window
			--create l_jeu.make
			create player.make (window.renderer, "joueur.jpg")

		end

	on_clean
			-- Cette m�thode est lanc� apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do

			text_library.quit_library
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	x_normal_test
			-- Test la posibiliter de modifier le x avec un cas normal
		note
			testing:  "covers/{SOMNAMBULE}.x", "execution/isolated", "execution/serial"
		do
			player.set_x (100)
			assert("{SOMNAMBULE}.`x' normal n'est pas valide (100).", player.x = 100)
			player.set_x (511)
			assert("{SOMNAMBULE}.`x' normal n'est pas valide (511).", player.x = 511)
		end



	x_limit_test
			-- Test les cas limite position horizontal
		note
			testing:  "covers/{SOMNAMBULE}.x", "execution/isolated", "execution/serial"
		do
			player.set_x (100)
			player.set_x (0)
			assert("{SOMNAMBULE}.`x' limit n'est pas valide (0).", player.x = 0)
		end

	x_errone_test
			-- Test des cas erron�s de la gestion de positionnement horizontal
		note
			testing:  "covers/{SOMNAMBULE}.x", "execution/isolated", "execution/serial/graphics"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				player.set_x (-123)
				assert("{SOMNAMBULE}.`x' erron� non g�rer", false)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end



		y_normal_test
				--  Test la posibiliter de modifier le y avec un cas normal
			note
				testing:  "covers/{SOMNAMBULE}.y", "execution/isolated", "execution/serial"
			do
				player.set_y (100)
				assert("{SOMNAMBULE}.`y' normal n'est pas valide (100).", player.y = 100)
				player.set_y (511)
				assert("{SOMNAMBULE}.`y' normal n'est pas valide (511).", player.y = 511)
			end



		y_limit_test
				--  Test la posibiliter de modifier le y avec un cas limit
			note
				testing:  "covers/{SOMNAMBULE}.y", "execution/isolated", "execution/serial"
			do
				player.set_y (100)
				player.set_y (0)
				assert("{SOMNAMBULE}.`y' limit n'est pas valide (0).", player.y = 0)
			end

		y_errone_test
				--  Test la posibiliter de modifier le y avec un cas erroner
			note
				testing:  "covers/{SOMNAMBULE}.y", "execution/isolated", "execution/serial"
			local
				l_retry:BOOLEAN
			do
				if not l_retry then
					player.set_y (-123)
					assert("{SOMNAMBULE}.`y' erron� non g�rer", False)
				end
			rescue
				if
					attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
					la_exception.code = {EXCEP_CONST}.precondition and then
					la_exception.type_name ~ Current.generating_type.name
				then
					l_retry:= True
					retry
				end
			end


		left_update_normal_test
				-- Test les cas normal update
			note
				testing:  "covers/{SOMNAMBULE}.go_left","covers/{SOMNAMBULE}.update","covers/{SOMNAMBULE}.stop_left", "execution/isolated", "execution/serial"
			do
				player.set_x (500)
				player.go_left (10)
				player.update (20)
				player.update (120)
				print(player.x.out)
				assert("{SOMNAMBULE}.`x' limit n'est pas valide (player.x < 500).", player.x < 500)

				player.stop_left
				assert("{SOMNAMBULE}.`x' limit n'est pas valide (player.x < 500).", player.x < 500)

			end



		set_deplacement_normal_test
				-- Test les cas normal de d'eplacement
			note
				testing:  "covers/{SOMNAMBULE}.set_deplacement", "execution/isolated", "execution/serial"
			do
				player.set_deplacement (1)
				assert("{SOMNAMBULE}.`y' limit n'est pas valide (0).", player.deplacement =1)
			end



		go_left_normal_test
			--Test d'un car normal de `go_left'
			note
				testing:  "covers/{SOMNAMBULE}.go_left", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_left (10)
				assert("{SOMNAMBULE}.`go_left' ln'est pas vrais.", player.going_left = true)
			end

		go_right_normal_test
			--Test d'un car normal de `go_right'
			note
				testing:  "covers/{SOMNAMBULE}.go_right", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_right (10)
				assert("{SOMNAMBULE}.`go_right' n'est pas vrais.", player.going_right = true)
			end
		go_up_normal_test
			--Test d'un car normal de `go_up'
			note
				testing:  "covers/{SOMNAMBULE}.go_up", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_up (10)
				assert("{SOMNAMBULE}.`go_up' n'est pas vrais.", player.going_up = true)
			end

		go_down_normal_test
			--Test d'un car normal de `go_down'
			note
				testing:  "covers/{SOMNAMBULE}.go_down", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_down (10)
				assert("{SOMNAMBULE}.`go_down' n'est pas vrais.", player.going_down = true)
			end


		stop_up_normal_test
			--Test d'un car normal de `stop_up'
			note
				testing:  "covers/{SOMNAMBULE}.stop_up", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_up (10)
				player.stop_up
				assert("{SOMNAMBULE}.`stop_up' n'est pas vrais.", player.going_up = false)
			end

		stop_down_normal_test
			--Test d'un car normal de `stop_down'
			note
				testing:  "covers/{SOMNAMBULE}.stop_down", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_down (10)
				player.stop_down
				assert("{SOMNAMBULE}.`stop_down' n'est pas vrais.", player.going_down = false)
			end
		stop_right_normal_test
			--Test d'un car normal de `stop_right'
			note
				testing:  "covers/{SOMNAMBULE}.stop_right", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_right (10)
				player.stop_right
				assert("{SOMNAMBULE}.`stop_right' n'est pas vrais.", player.going_right = false)
			end
		stop_left_normal_test
			--Test d'un car normal de `stop_left'
			note
				testing:  "covers/{SOMNAMBULE}.stop_left", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_left (10)
				player.stop_left
				assert("{SOMNAMBULE}.`stop_left' n'est pas vrais.", player.going_left = false)
			end

		inverse_mouvement_normal_test
				--Test d'un car normal de `inverse_mouvement'
			note
				testing:  "covers/{SOMNAMBULE}.inverse_mouvement", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_left (10)
				player.inverse_mouvement
				assert("{SOMNAMBULE}.`going_right' n'envois pas a droite.", player.going_right = true)
				player.inverse_mouvement
				assert("{SOMNAMBULE}.`going_left' n'envois pas a gauche.", player.going_left = true)
				player.stop_left
				player.go_up (30)
				player.inverse_mouvement
				assert("{SOMNAMBULE}.`going_down' n'envois pas en bas.", player.going_down = true)
				player.inverse_mouvement
				assert("{SOMNAMBULE}.`going_up' n'envois pas en haut.", player.going_up = true)

			end


		set_cycle_to_move_normal_test
				--Test d'un car normal de `set_cycle_to_move'
			note
				testing:  "covers/{SOMNAMBULE}.set_cycle_to_move", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_left (10)
				player.set_cycle_to_move (4)
				assert("{SOMNAMBULE}.`cycle_to_move' le cycle na pa bien ete setter", player.cycle_to_move = 4)
			end

		update_normal_test

			note
				testing:  "covers/{SOMNAMBULE}.set_cycle_to_move", "execution/isolated", "execution/serial"
			do
				player.set_x (100)
				player.set_y (100)
				player.go_right (10)
				player.set_cycle_to_move (4)
				player.update (10)
				assert("{SOMNAMBULE}.`cycle_to_move' n'envois pas a droite.", player.x = 100)
				player.update (20)
				player.update (30)
				player.update (40)
				player.update (50)
				player.update (60)
				assert("{SOMNAMBULE}.`cycle_to_move' n'envois pas a droite.", player.x = 101)
			end

		update_errone_test
				--  Test la posibiliter de deplacer avec un update erroner
			note
				testing:  "covers/{SOMNAMBULE}.update", "execution/isolated", "execution/serial"
			local
				l_retry:BOOLEAN
			do
				if not l_retry then
					player.set_x (100)
					player.set_y (100)
					player.go_right (10)
					player.set_cycle_to_move (0)
					player.update (20)
					player.update (10)
					assert("{SOMNAMBULE}.`update' le update plus peti le x devrais pas changer",player.x = 101)

				end
			rescue
				if
					attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
					la_exception.code = {EXCEP_CONST}.precondition and then
					la_exception.type_name ~ Current.generating_type.name
				then
					l_retry:= True
					retry
				end
			end

		set_positions_normal_test
			note
				testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"
			do
				player.set_positions (100, 100)
				assert("{SOMNAMBULE}.`x' est positife.", player.x = 100)
				assert("{SOMNAMBULE}.`y' est positife.", player.y = 100)

			end
		set_positions_erroner_test
			note
				testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"

			local
				l_retry:BOOLEAN
			do
				if not l_retry then
					player.set_positions (-100, -100)
					assert("{SOMNAMBULE}.`set_positions'le x et y ne devrais pas autoriser set_positions(-100,-100) ",false)

				end
			rescue
				if
					attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
					la_exception.code = {EXCEP_CONST}.precondition and then
					la_exception.type_name ~ Current.generating_type.name
				then
					l_retry:= True
					retry
				end
			end

		set_positions_limite_test
			note
				testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"
			do
				player.set_positions (0, 0)
				assert("{SOMNAMBULE}.`set_positions'le x et y devrais autoriser set_positions(0,0) ",player.x= 0)
				assert("{SOMNAMBULE}.`set_positions'le x et y devrais autoriser set_positions(0,0) ",player.y= 0)
			end

feature {NONE}


	window:GAME_WINDOW_RENDERED
			-- La {GAME_WINDOW} utilis� pour g�n�rer `player'

	player:SOMNAMBULE
			--L'objet tester dans `Current'



feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"



end


