note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "2020-06-07"
	testing: "type/manual"

class
	MEUBLE_DEPLACABLE_TEST

inherit
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			audio_library.enable_playback
			game_library.enable_video -- Enable the video functionalities
			image_file_library.enable_image (true, false, false)
			create l_window_builder
			l_window_builder.set_dimension (1500, 700)
			window := l_window_builder.generate_window

			create meuble.make (window.renderer, "tabouret.jpg")

		end

	on_clean
			-- Cette m�thode est lanc� apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do

			text_library.quit_library
			image_file_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	rotate_test
			-- New test routine
		note
			testing:  "execution/isolated", "covers/{MEUBLE_DEPLACABLE}.rotate", "execution/serial"
		do
			meuble.send_right := true
			meuble.rotate

			assert("{MEUBLE_DEPLACABLE}.`send_up' normal n'est pas valide.", meuble.send_up = true)
			meuble.rotate
			assert("{MEUBLE_DEPLACABLE}.`send_left' normal n'est pas valide.", meuble.send_left = true)
			meuble.rotate
			assert("{MEUBLE_DEPLACABLE}.`send_down' normal n'est pas valide.", meuble.send_down = true)
			meuble.rotate
			assert("{MEUBLE_DEPLACABLE}.`send_down' normal n'est pas valide.", meuble.send_right = true)

		end

	default_send_test
			-- New test routine
		note
			testing:  "covers/{MEUBLE}.default_send", "execution/isolated", "execution/serial"
		do
			meuble.set_x (500)
			meuble.default_send

			assert("{MEUBLE}.`default_send' normal n'est pas valide ne revoit pas dans d'autre direction que `send_back'.", meuble.send_back = true)
		end


	set_send_back_test
			--Test d'un cas normal de de comment renvoyer le somnabule
		note
			testing:  "covers/{MEUBLE}.set_send_back", "execution/isolated", "execution/serial"
		do
			meuble.set_x (500)
			meuble.set_send_back (true)

			assert("{MEUBLE}.`default_send' normal ne fait que renvoyer `send_back'.", meuble.send_back = true)
		end

	set_send_right_test
			--Test d'un cas normal de de comment envoyer le somnabule a droite
		note
			testing:  "covers/{MEUBLE}.set_send_right", "execution/isolated", "execution/serial"
		do
			meuble.set_x (500)
			meuble.set_send_right (true)

			assert("{MEUBLE}.`default_send' normal envoi vers la droite `send_back'.", meuble.send_right = true)
		end

	set_send_left_test
			--Test d'un cas normal de de comment envoyer le somnabule a gauche
		note
			testing:  "covers/{MEUBLE}.set_send_left", "execution/isolated", "execution/serial"
		do
			meuble.set_x (500)
			meuble.set_send_left (true)

			assert("{MEUBLE}.`set_send_left' normal n'est pas valide ne revoi pas avec `send_left'.", meuble.send_left = true)
		end


	set_send_up_test
			--Test d'un cas normal de de comment envoyer le somnabule en haut
		note
			testing:  "covers/{MEUBLE}.set_send_up", "execution/isolated", "execution/serial"
		do
			meuble.set_x (500)
			meuble.set_send_up (true)

			assert("{MEUBLE}.`set_send_up' normal n'est pas valide ne revoi pas avec `send_back'.", meuble.send_up = true)
		end

	set_send_down_test
			--Test d'un cas normal de de comment envoyer le somnabule en bas
		note
			testing:  "covers/{MEUBLE}.set_send_down", "execution/isolated", "execution/serial"
		do
			meuble.set_x (500)
			meuble.set_send_down (true)

			assert("{MEUBLE}.`set_send_down' normal n'est pas valide ne revoi pas avec `send_back'.", meuble.send_down = true)
		end


	set_positions_normal_test
		note
			testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"
		do
			meuble.set_positions (100, 100)
			assert("{SOMNAMBULE}.`x' est positife.", meuble.x = 100)
			assert("{SOMNAMBULE}.`y' est positife.", meuble.y = 100)

		end
	set_positions_erroner_test
		note
			testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"

		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				meuble.set_positions (-100, -100)
				assert("{SOMNAMBULE}.`set_positions'le x et y ne devrais pas autoriser set_positions(-100,-100) ",false)

			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	set_positions_limite_test
		note
			testing:  "covers/{SOMNAMBULE}.set_positions", "execution/isolated", "execution/serial"
		do
			meuble.set_positions (0, 0)
			assert("{SOMNAMBULE}.`set_positions'le x et y devrais autoriser set_positions(0,0) ",meuble.x= 0)
			assert("{SOMNAMBULE}.`set_positions'le x et y devrais autoriser set_positions(0,0) ",meuble.y= 0)
		end


feature {NONE}
	meuble:MEUBLE_DEPLACABLE
			--Meuble deplacable pour les test

	window:GAME_WINDOW_RENDERED
		-- La {GAME_WINDOW} utilis� pour g�n�rer `meuble'

end


