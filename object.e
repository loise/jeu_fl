note
	description: "Classe {OBJECT} definire tous les parametre de basse des object ."
	author: "Jonathan Parent"
	date: "2020-04-21"


deferred class
	OBJECT

inherit
	GAME_TEXTURE
		rename
			make as make_texture
		end


feature{NONE} --Constructeur

	make(a_renderer:GAME_RENDERER;a_nom_photo:STRING_32)
			--Sert a cr�e un object en affichant le `a_nom_photo' dans le `a_renderer'
		local
			l_image:IMG_IMAGE_FILE
		do
			deplacement:=0
			is_movable:=false
			has_error := False
			can_move_object:=false
			nom_photo:= a_nom_photo

			create l_image.make (a_nom_photo)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then

					make_from_image (a_renderer, l_image)
					if not has_error then
						sub_image_width := width
						sub_image_height := height

					end
				else
					has_error := True
				end
			else
				has_error := True
			end

		end



feature --Access
	effect
		--Ce qui arive lorsque `Current' doit jouer un effet de son si il existe
		--et retourne un chifre qui va dire de combien le `SOMNAMBULE' a �t� deranger
		do

		end

	have_colide
		--Ce qui arive lorsque `Current' entre en colision
		do

		end
	inverse_mouvement
		--Inverse le mouvement de `Current'
		do

		end


	stop_all
			--Stop `Current' toute les direction
		do

			going_down:= false
			going_up:= false
			going_right:= false
			going_left:= false
		end

--	assigne_le_mouvement(a_object:OBJECT)
--			--Sert a
--		do
--			a_object.going_right := going_right
--			a_object.going_up := going_up
--			a_object.going_down := going_down
--			a_object.going_left := going_left
--		end


	get_x2:INTEGER_32
			--Sert a avoir la position de x2 de `Current' selont l'image
		do
			Result:=x+width
		end

	get_y2:INTEGER_32
			--Sert a avoir la position de y2 de `Current' selont l'image
		do
			Result:=y+height
		end

	set_positions(a_x,a_y:INTEGER_32)
			--Set a asigner les position pour `Current' de `x' avec `a_x' et de `y' avec `a_y'
		require
			X_Positive: a_x >= 0
			Y_Positive: a_y >= 0
		do
			set_x (a_x)
			set_y (a_y)
		ensure
			A_assign_x: x = a_x
			A_assign_y: y = a_y
		end



feature --Constant

	nom_photo:STRING_32
			--Est le nom de l'image dans le fichier

	sub_image_width, sub_image_height:INTEGER
			-- Dimension of the portion of image to show inside `surface'

	x:INTEGER assign set_x
			-- Vertical position of `Current'

	y:INTEGER assign set_y
			-- Horizontal position of `Current'

	set_x(a_x:INTEGER)
			-- Assign la valeur de `x' avec `a_x'
		require
			X_Positive: a_x >= 0
		do
			x := a_x
		ensure
			Is_Assign: x = a_x
		end

	set_y(a_y:INTEGER)
			-- Assign la valeur de `y' avec `a_y'
		require
			Y_Positive: a_y >= 0
		do
			y := a_y
		ensure
			Is_Assign: y = a_y
		end

-----------------------------------------------
	going_right:BOOLEAN assign set_going_right
			--Savoire si `Current' va a droite
--		deferred
--		end
	going_left:BOOLEAN assign set_going_left
			--Savoire si `Current' va a gauche
--		deferred
--		end
	going_up:BOOLEAN assign set_going_up
			--Savoire si `Current' monte
--		deferred
--		end
	going_down:BOOLEAN assign set_going_down
			--Savoire si `Current' descent
--		deferred
--		end

	set_going_right(a_going_right:BOOLEAN)
			--Assigne
		do
			going_right := a_going_right
		ensure
			Is_Assign: going_right = a_going_right
		end

	set_going_up(a_going_up:BOOLEAN)
			--Assigne
		do
			going_up := a_going_up
		ensure
			Is_Assign: going_up = a_going_up
		end

	set_going_left(a_going_left:BOOLEAN)
			--Assigne
		do
			going_left := a_going_left
		ensure
			Is_Assign: going_left = a_going_left
		end

	set_going_down(a_going_down:BOOLEAN)
			--Assigne
		do
			going_down := a_going_down
		ensure
			Is_Assign: going_down = a_going_down
		end
---------------------------------------------------


	can_move_object:BOOLEAN assign set_can_move_object
			--Connaitre si `Current' va �tre capable de d�placer les object

	is_movable:BOOLEAN
		--Connaitre si on peu bouger `Current'

	old_timestamp:NATURAL_32
			-- When appen the last movement (considering `movement_delta')


	deplacement:INTEGER_32 assign set_deplacement
			--Indique la longeur du dernier `deplacement' de `Current'

	set_deplacement(a_deplacement:INTEGER_32)
			--Assigne
		do
			deplacement := a_deplacement
		ensure
			Is_Assign: deplacement = a_deplacement
		end



	set_can_move_object(a_can_move_object:BOOLEAN)
		--Assigne
		do
			can_move_object := a_can_move_object
		ensure
			Is_Assign: can_move_object= a_can_move_object
		end


feature {NONE} -- constants



	movement_delta:NATURAL_32 = 10
			-- The delta time between each movement of `Current'




feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end




