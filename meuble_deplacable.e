note
	description: "Classe {MEUBLE_DEPLACABLE} est un meuble qui a la posibliliter d'�tre d�placer."
	author: "Jonathan Parent"
	date: "2020-04-21"


 class
	MEUBLE_DEPLACABLE


inherit
	MEUBLE
		rename
			make as make_meuble
		end
	INTERACTION


create
	make

feature{NONE}--Constructeur
	make(a_renderer:GAME_RENDERER;a_nom_photo:STRING_32)
			--Sert a cr�e un meuble selon les atribut
		do
			make_meuble (a_renderer, a_nom_photo)
			is_movable:=true
		end



feature --metre a jour
	update(a_player:JOUEUR)
			--Calcule la nouvelle position de `x' et de `y' selont `a_player'
		do
			if is_selected then
				x := a_player.x - distance_x
				y := a_player.y - distance_y
				deplacement := a_player.deplacement

			end
		end


feature
	can_move(a_player:JOUEUR):BOOLEAN
			--Tester le deplacement de `current' selon si le `a_player' est suvisament proche
		local
			l_joueur_dans_la_zone:BOOLEAN
		do
			l_joueur_dans_la_zone:=false

			if (a_player.x - x) < 10 or (a_player.x -(width + x) ) < 60 then

				distance_x := a_player.x - x
				distance_y := a_player.y - y
				l_joueur_dans_la_zone:=true
			end


			Result :=l_joueur_dans_la_zone
		end

	rotate
			--Pour tourner `Current'
		do
			rotation := rotation+1
			if not send_back then
				if send_down then  --  v = >
					send_down:=false
					send_right:=true
					print("Right:")
				elseif send_right then -- > = ^
					send_right:=false
					send_up:= true
					print("Up:")
				elseif send_up then  -- ^ = <
					send_up:=false
					send_left:=true
					print("Left:")
				elseif send_left then  -- < = v
					send_left:=false
					send_down:=true
					print("Down:")
					rotation:=0
				end
			end
		end



	selecte --Select mot reserver
			--Active le deplacement de `current'
		do
			is_selected:=true
		end

	deselect
			--Desactiver le deplacement de `current'
		do
			is_selected:=false
		end


feature --Valeur  -MODIIIFER-------------------------modifier-----modifier------modirf


	distance_x:INTEGER_32
			--Distance en x entre le `JOUEUR' et `current'

	distance_y:INTEGER_32
			--Distance en y entre le `JOUEUR' et `current'




--	position_dans_la_liste:INTEGER_32 assign set_position_dans_la_liste
--			--Sert a connaitre la position de `current' dans la liste

--	set_position_dans_la_liste(a_index:INTEGER_32)
--			--Assigne la position de `current' dans la liste
--		do
--			position_dans_la_liste := a_index
--		ensure
--			Is_Assign: position_dans_la_liste = a_index
--		end


feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end





