note
	description: "Classe{SONS} pour permet d'utiliser un sont et de le faire jouer."
	author: "Jonathan Parent"
	date: "2020-05-01"


deferred class
	SONS


inherit
	AUDIO_LIBRARY_SHARED




feature {NONE}
	make_son(a_nom_music:STRING_32)
			--Sert a cr�e un sont pour `Current'
		do
			create {AUDIO_SOUND_FILE}sound.make (a_nom_music)
			audio_library.sources_add
			sound_source:=audio_library.last_source_added	-- The second source will be use for playing the space sound
			if sound.is_openable then
				sound.open
			else
				print("Cannot open sound files.")

			end
		end

	sound:AUDIO_SOUND
			-- To play when the user interact with the program



	sound_source:AUDIO_SOURCE
			-- The source to play the `sound'


feature
	play_sound
			--Fait jouer le sons de `Current'
		do
			if sound.is_open then
				if not sound_source.is_playing  then
					sound.restart
					sound_source.queue_sound (sound)	-- Queud the sound into the source queue
					sound_source.play
				end

			end

		end





feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end




