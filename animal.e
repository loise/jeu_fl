note
	description: "Sert a cr�e un {ANIMAL} un animale est tout ce qui a la capaciter de se deplacer et faire du son ."
	author: "Jonathan Parent"
	date: "2020-04-21"


class
	ANIMAL

inherit
	GAME_LIBRARY_SHARED

	PERSONAGE

		rename
			make as make_personage
		redefine
			effect
		end
	SONS



create
	make

feature {NONE} --Initialization
	make(a_renderer:GAME_RENDERER;a_nom_photo,a_nom_son:STRING_32)
			--Sert a cree `Current' en afichant `a_nom_photo' dans le `a_renderer'
		do
			make_personage (a_renderer, a_nom_photo)
			can_move_object:=false
			is_movable:=false
			make_son (a_nom_son)
			go_right (1)	--initier le deplacement ver la droit
			--nom:=a_nom_son
			vecteur_de_vitesse:=1
			point_a := [100,44]
			point_b := [500,44]
			set_cycle_to_move (1)
		end



feature


	effect
			--Sert a fair jouer un effet son de `Current'
		do
			if can_make_noise then
				play_sound
			end


		end



	patrole_a_to_b(a_timestamp: NATURAL_32)
			--Gere la gestion de `Current' selon `point_a' et `point_b'
		do
			update (a_timestamp)

			if (x <= point_a.x) and not going_right then
				print("droit")
				stop_all
				go_right (a_timestamp)
			end

			if (x >= point_b.x) and not going_left then
				print("gauche")
				stop_all
				go_left (a_timestamp)
			end
		end

feature	--Assignation

	can_make_noise:BOOLEAN assign set_can_make_noise

	set_can_make_noise(a_can_make_noise :BOOLEAN)
			-- Assign the value of `x' with `a_x'

		do
			can_make_noise := a_can_make_noise
		ensure
			Is_Assign: can_make_noise = a_can_make_noise
		end

	point_a:TUPLE [x,y:INTEGER_32] assign set_point_a
			--Les point au quelle `Current' va aller

	point_b:TUPLE [x,y:INTEGER_32] assign set_point_b
			--Les point au quelle `Current' va aller


	set_point_a(a_coordoner :TUPLE[x,y:INTEGER_32])
			-- Assign the value of `x' with `a_x'
		require
			X_Positive: a_coordoner.x >= 0
			X_Positive: a_coordoner.y >= 0
		do
			point_a := a_coordoner
		ensure
			Is_Assign: point_a = a_coordoner
		end

	set_point_b(a_coordoner :TUPLE[x,y:INTEGER_32])
			-- Assign the value of `x' with `a_x'
		require
			X_Positive: a_coordoner.x >= 0
			X_Positive: a_coordoner.y >= 0
		do
			point_b := a_coordoner
		ensure
			Is_Assign: point_b = a_coordoner
		end



feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"
end



