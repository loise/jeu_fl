note
	description: "Classe {MEUBLE} sert a definire le caract�ristique des meuble  ."
	author: "Jonathan Parent"
	date: "2020-04-21"


deferred class
	MEUBLE

inherit
	OBJECT
		rename
			make as make_object
		end

	COLLISABLE



feature {NONE} --Constructeur
	make(a_renderer:GAME_RENDERER;a_nom_photo:STRING_32)
			--Cr�e  un object meuble
		do
			print("AA")
			make_object(a_renderer,a_nom_photo)
			deplacement:=0
			can_move_object:= true
			is_selected:=false
			rotation:=0
			default_send
		end

feature --Fonctionale
	default_send
			--Sert a donner a des object un �tat de basse a `Current'
		do
			send_back:=true
			send_down:=false
			send_left:=false
			send_right:=false
			send_up:=false
		end


	reset_send
			--Remete les boolean SEND de `Current' a false
		do
			send_back:=false
			send_down:=false
			send_left:=false
			send_right:=false
			send_up:=false
		end






feature

	rotation:INTEGER_32
			--lorseque on va fair un rotation de 90 degreg  ver la gauche on augmente `Current' et debute a 0


	send_back:BOOLEAN assign set_send_back
			--Indicte si on doit renvoyer `Current' de ou il vient
	send_down:BOOLEAN assign set_send_down
			--Indicte si on doit envoyer `Current' ver le bas
	send_up:BOOLEAN assign set_send_up
			--Indicte si on doit envoyer `Current' ver le haut
	send_right:BOOLEAN assign set_send_right
			--Indicte si on doit envoyer `Current' ver la droite
	send_left:BOOLEAN assign set_send_left
			--Indicte si on doit envoyer `Current' ver la gauche


	set_send_back(a_send_back:BOOLEAN)
			--Assigner `send_back'
		do
			reset_send
			send_back := a_send_back
		ensure
			Is_Assign: send_back = a_send_back
		end

	set_send_down(a_send_down:BOOLEAN)
			--Assigner `send_down'
		do
			reset_send
			send_down := a_send_down
		ensure
			Is_Assign: send_down = a_send_down
		end

	set_send_up(a_send_up:BOOLEAN)
			--Assigner `send_up'
		do
			reset_send
			send_up := a_send_up
		ensure
			Is_Assign: send_up = a_send_up
		end

	set_send_right(a_send_right:BOOLEAN)
			--Assigner `send_right'
		do
			reset_send
			send_right := a_send_right
		ensure
			Is_Assign: send_right = a_send_right
		end

	set_send_left(a_send_left:BOOLEAN)
			--Assigner `send_left'
		do
			reset_send
			send_left := a_send_left
		ensure
			Is_Assign: send_left = a_send_left
		end


	is_selected:BOOLEAN
			--Pour activer le de deplacement de `Current'

feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end




