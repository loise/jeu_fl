note
	description: "Classe {MAP} sert a gerer le placement de object dans la map."
	author: "Jonathan Parent"
	date: "2020-04-21"


class
	MAP

inherit
	GAME_LIBRARY_SHARED

create
	make


feature
	make(a_renderer:GAME_RENDERER;a_timestamp:NATURAL_32)
			--Sert a cree les object qui seront utiliser
		do
			niveaut := 1

			create liste_object.make (10) --cree la liste des object

			create background.make (a_renderer,"plancer_bois.jpg")
			create you_lose_scren.make (a_renderer,"you_lose.jpg")
			create hud.make (a_renderer,"hud.png")
			create table.make (a_renderer, "table.jpg")
			create chaise.make (a_renderer, "chaise.jpg")
			create chaise2.make (a_renderer, "chaise.jpg")
			create tabouret.make(a_renderer,"tabouret.jpg")
			create somnabule.make(a_renderer, "somnambulle.jpg")
			create porte1.make (a_renderer, "porte.jpg")
			create lit.make(a_renderer)
			create chien.make (a_renderer,"chien.jpg","moli_qui_jap.wav")
			create joueur.make (a_renderer,"joueur.jpg")
			--map1(a_timestamp)
		end



feature --Fonctionale

	map0(a_timestamp:NATURAL_32)
			--invoker les objet de la map0
		do
			liste_object.wipe_out

			lit.set_positions (800, 200)

			table.set_positions (105, 500)

			porte1.set_positions (1200,400)

			chaise.set_positions (403, 204)
			chaise2.set_positions (283,204)
			tabouret.set_positions (720,450)

			somnabule.set_positions (10, 200)
			somnabule.stop_all
			somnabule.se_deplace (a_timestamp)

			chien.x := 200
			chien.y:=50
			chien.can_make_noise:=false
			joueur.set_x (944)
			joueur.set_y (400)

			liste_object.start

			liste_object.extend (lit)
			liste_object.extend (chaise)
			liste_object.extend (chaise2)
			liste_object.extend (tabouret)
			liste_object.extend (table)


			tabouret.reset_send
			tabouret.send_down:= true

			chaise.send_right:=true
		end

	map1(a_timestamp:NATURAL_32)
			--invoker les objet de la map1
		do
			liste_object.wipe_out

			lit.set_positions (500, 500)

			table.set_positions (50, 500)

			porte1.set_positions (1200,400)

			chaise.set_positions (303, 404)
			chaise2.set_positions (583,104)
			tabouret.set_positions (720,450)

			somnabule.set_positions (10, 200)
			somnabule.stop_all
			somnabule.se_deplace (a_timestamp)

			chien.x := 200
			chien.y:=50
			joueur.set_x (944)
			joueur.set_y (400)

			liste_object.start

			liste_object.extend (lit)
			liste_object.extend (chaise)
			liste_object.extend (chaise2)
			liste_object.extend (tabouret)
			liste_object.extend (chien)
			liste_object.extend (table)
			liste_object.extend (porte1)

			tabouret.reset_send
			tabouret.send_down:= true

			chaise.send_right:=true
		end

	map2(a_timestamp:NATURAL_32)
			--invoker les objet de la map2
		do
			liste_object.wipe_out

			lit.set_positions (1300, 200)
			table.set_positions (500, 200)

			porte1.set_positions (1200,400)


			chaise.set_positions (403, 404)
			chaise2.set_positions (283,404)
			tabouret.set_positions (720,450)

			somnabule.set_positions (10, 200)
			somnabule.stop_all
			somnabule.se_deplace (a_timestamp)

			chien.x := 200
			chien.y:=50
			chien.can_make_noise:=true
			joueur.set_x (944)
			joueur.set_y (400)

			liste_object.start

			liste_object.extend (lit)
			liste_object.extend (chaise)
			liste_object.extend (chaise2)
			liste_object.extend (tabouret)
			liste_object.extend (chien)
			liste_object.extend (table)
			liste_object.extend (porte1)

			tabouret.reset_send
			tabouret.send_down:= true

			chaise.send_right:=true
		end

	chaise:MEUBLE_DEPLACABLE
			--chaise
	chaise2:MEUBLE_DEPLACABLE
			--un object`MEUBLE_DEPLACABLE'

	tabouret:MEUBLE_DEPLACABLE
			--un object`MEUBLE_DEPLACABLE'

	porte1:MEUBLE_NON_DEPLACABLE
			--Porte

	chien:ANIMAL
			--un object `ANIMAL'
	somnabule:SOMNAMBULE
			--un object `SOMNAMBULE'

	niveaut:INTEGER_32
			--sert a dire a quelle niveut on est rendu(la map)

	table:MEUBLE_NON_DEPLACABLE
			--un object `MEUBLE_NON_DEPLACABLE' pour une table

	lit:LIT
			--un object `LIT'

	joueur:JOUEUR
			--un object `JOUEUR'

	background: OBJECT_INERTE
			--un object `OBJECT_INERTE' pour le fond

	you_lose_scren:OBJECT_INERTE
			--un object `OBJECT_INERTE' pour le fond lorsque le joueur perd

	hud:OBJECT_INERTE
			--un object `OBJECT_INERTE' du HUD

	liste_object: ARRAYED_LIST [OBJECT]
		--Une list des object cr�e selont et les ajoute a la liste selont la map

feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end



