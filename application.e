note
    description : "Le jeut de Somnbule"
    author      : "Jonathan Parent"
    generator   : "Eiffel Game2 Project Wizard"
    date        : "2020-04-21"
    revision    : "0.11"

class
    APPLICATION

inherit
	AUDIO_LIBRARY_SHARED
	GAME_LIBRARY_SHARED		-- To use `game_library'
	TEXT_LIBRARY_SHARED		-- To use `text_library'
	IMG_LIBRARY_SHARED		-- To use `image_file_library'

create
    make

feature {NONE} -- Initialization

    make
		-- Run application.
		local
			l_jeu:ENGINE
		do
			audio_library.enable_playback
			game_library.enable_video -- Enable the video functionalities
			image_file_library.enable_image (true, false, false)  -- Enable PNG image (but not TIF or JPG).
			create l_jeu.make
			if not l_jeu.has_error then
				l_jeu.run
			end
		end

feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end






