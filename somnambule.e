note
	description: "Summary description for {SOMNAMBULE}."
	author: "Jonathan Parent"
	date: "2020-05-27"


class
	SOMNAMBULE

inherit
	PERSONAGE
		rename
			make as make_personage
		redefine
			have_colide
		end


create
	make

feature
	make(a_renderer:GAME_RENDERER;a_nom_photo:STRING_32)
			--Sert a cr�e `Current'
		do
			make_personage(a_renderer,a_nom_photo)
			vecteur_de_vitesse := 1
			can_move_object:=false
			is_movable:=false
			reveil_point:= 10
			set_cycle_to_move (2)
		end



feature



	se_deplace(a_time: NATURAL_32)
		--Donne a `Current' un d�placment de basse en debut de niveaut
		do
			go_right (a_time)
		end




	reveil_point:INTEGER_32 assign set_reveil_point
			--Nombre de point de reveile de `Current'
	set_reveil_point(a_reveil_point:INTEGER_32)
			--Sert a assigner les valeur de `set_reveil_point'
		do
			reveil_point := a_reveil_point
		ensure
			Is_Assign: reveil_point = a_reveil_point
		end


	have_colide
			--Lorsque il a une colision on reduit le nobre de poin de reveile de `Current'
		do
			reveil_point:= reveil_point-1
		end



feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"
end



