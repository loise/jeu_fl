note
	description: "Sert a definre un object comme colisable {COLLISABLE} et tester la colison entre deux object COLLISABLE."
	author: "Jonathan Parent"
	date: "2020-04-21"


deferred class
	COLLISABLE



feature --Fonctionel
	has_colide(a_object: COLLISABLE;a_meuble_colision: detachable COLLISABLE) :BOOLEAN
			--tester la colistion `a_object' et la teste avec `Current'
		--local
			--l_colision:BOOLEAN
		do
			if tester_bordure(a_object) then
				Result:= true
			end

			if x + width > a_object.x then
				if x < a_object.x + a_object.width then
					if y < a_object.y + a_object.height then
						if y + height > a_object.y then			--Condition de basse
							if a_object /= Current then
								Result:=true
							end
						end
					end
				end
			end
				--Result:=false
		end


	tester_bordure(a_object: COLLISABLE):BOOLEAN
			--Permet d'empecher `a_object' de sortire des limte
		local
			l_result:BOOLEAN
		do
			l_result:= false
			if a_object.x <=1 then
				a_object.x:=1
				l_result:= true
			end
			if a_object.y <=1 then
				a_object.y:=1
				l_result:= true
			end
			if a_object.get_y2 >=700 then
				a_object.y:=700 - a_object.height
				l_result:= true
			end
			if a_object.get_x2 >=1500 then
				a_object.x :=1500- a_object.width
				l_result:= true
			end
			Result := l_result
		end



	calcule_le_mouvement(a_object: OBJECT)
				--Donne `a_object' un deplacement selon la `direction'
		do
			if  going_right then 		--go_right
				a_object.x := a_object.x + a_object.deplacement
			end
			if  going_left then 		--go_left
				a_object.x := a_object.x - a_object.deplacement
			end
			if going_up then 			--go_up
				a_object.y := a_object.y - a_object.deplacement
			end
			if  going_down  then 	--go_down
				a_object.y := a_object.y + a_object.deplacement
			end

		end





	anuler_mouvement(a_object_mouve:COLLISABLE)
			--Sert a annuler le mouvement de `Current' selont le dernier mouvement qu'il a fait
		do

			if a_object_mouve.going_up then 			--go_up
				Current.y := Current.y + a_object_mouve.deplacement
			end
			if  a_object_mouve.going_down then 	--go_down
				Current.y := Current.y - a_object_mouve.deplacement
			end
			if  a_object_mouve.going_right then 		--go_right
				Current.x := Current.x - a_object_mouve.deplacement
			end
			if  a_object_mouve.going_left then 		--go_left
				Current.x := Current.x + a_object_mouve.deplacement
			end
		end





	x:INTEGER_32 assign set_x
			-- Horizontal position of `Current'
		deferred
		end

	set_x(a_x:INTEGER_32)
			-- Assign la valeur de `x' selont `a_x'
		deferred
		end
	y:INTEGER_32 assign set_y
			-- Vertical position of `Current'
		deferred
		end
	set_y(a_y:INTEGER_32)
			-- Assign la valeur de `y' selont `a_y'
		deferred
		end
	width:INTEGER_32
			--La largeur de limage de `Current'
		deferred
		end
	height:INTEGER_32
			--La hauteur de limage de `Current'
		deferred
		end

	get_x2:INTEGER_32
			--La largeur de `x' plus image
		deferred
		end
	get_y2:INTEGER_32
			--La largeur de `x' plus image
		deferred
		end



	effect
			--Sert a faire un effect particulier a `Current'
		deferred
		end
	have_colide
			--Dit si est entre en colision a la dernire verivication de `Current'
		deferred
		end
	inverse_mouvement
			--Sert a inverser le mouvement de `Current'
		deferred
		end




	deplacement:INTEGER_32 assign set_deplacement
		deferred
		end
	set_deplacement(a_deplacement:INTEGER_32)
		deferred
		end

	is_movable:BOOLEAN
		deferred
		end

	can_move_object:BOOLEAN
			--Savoir si `Current' peut deplacer des `MEUBLE_DEPLACABLE'
		deferred
		end
	going_right:BOOLEAN --assign set_going_right
			--Savoire si `Current' va a droite
		deferred
		end
	going_left:BOOLEAN
			--Savoire si `Current' va a gauche
		deferred
		end
	going_up:BOOLEAN
			--Savoire si `Current' monte
		deferred
		end
	going_down:BOOLEAN
			--Savoire si `Current' descent
		deferred
		end


	--item: OBJECT

--	meuble_colision: detachable COLLISABLE
--			--Le `meuble_colision' est un `OBJECT' `COLLISABLE' qui va reentrer dans la fonction `colide'


feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end


