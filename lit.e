note
	description: "Classe {LIT} Objective du jeu est de fair toucher le lit au somnabule ."
	author: "Jonathan Parent"
	date: "2020-04-21"

class
	LIT

inherit
	MEUBLE_NON_DEPLACABLE
	rename
		make as make_meuble_non_deplacable
	end

create
	make

feature {NONE}
	make(a_renderer: GAME_RENDERER)
			--Sert a cree un lit
		do
			make_meuble_non_deplacable (a_renderer, "Lit.jpg")
		end


	--peutere effect pour win le jeu

feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end



