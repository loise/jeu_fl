note
	description: "Classe {PERSONAGE} sert a ger� le les d�placement ."
	author: "Jonathan Parent"
	date: "2020-04-21"

deferred class
	PERSONAGE

inherit
	OBJECT
		redefine
			inverse_mouvement
		end

	COLLISABLE

feature--Fonctional

	update(a_timestamp:NATURAL_32)
			-- Update the surface depending on the present `a_timestamp'.
			-- Each 100 ms, the image change; each 10ms `Current' is moving
		require
			A_plus_grand : a_timestamp >= old_timestamp
		local
			l_delta_time:NATURAL_32

		do
			if going_left or going_right or going_up or going_down then

				l_delta_time := a_timestamp - old_timestamp
				if l_delta_time // movement_delta > 0 then
					if cycle_to_move_current >= cycle_to_move then
						cycle_to_move_current := 0


						deplacement := (l_delta_time // movement_delta).to_integer_32*vecteur_de_vitesse

						if going_right then

							x := x + (l_delta_time // movement_delta).to_integer_32*vecteur_de_vitesse
						end
						if going_left then

							x := x - (l_delta_time // movement_delta).to_integer_32*vecteur_de_vitesse
						end
						if going_up then

							y := y - (l_delta_time // movement_delta).to_integer_32*vecteur_de_vitesse
						end
						if going_down then

							y := y + (l_delta_time // movement_delta).to_integer_32*vecteur_de_vitesse
						end
						old_timestamp := old_timestamp + (l_delta_time // movement_delta) * movement_delta
					else
						old_timestamp := old_timestamp + (l_delta_time // movement_delta) * movement_delta
						cycle_to_move_current := cycle_to_move_current +1
					end
				end

			end
		end


	go_up (a_time:NATURAL_32)
			--Active a `Current' de monter
		do
			old_timestamp := a_time
			going_up := true

		ensure
			valid_going_up:going_up
		end

	go_down (a_time:NATURAL_32)
			--Active a `Current' de decendre
		do
			old_timestamp := a_time
			going_down:= true
		ensure
			valid_going_down: going_down
		end

	go_right (a_time:NATURAL_32)
			--Active a `Current' d'aller a droite
		do
			old_timestamp := a_time
			going_right:= true
		ensure
			valid_going_right: going_right
		end

	go_left (a_time:NATURAL_32)
			--Active a `Current' d'aller a gauche
		do
			old_timestamp := a_time
			going_left:= true
		ensure

			valid_going_left: going_left
		end


	stop_left
			--Stop `Current' d'aller a gauche
		do
			going_left:= false
		ensure
			valid_not_going_left: not going_left
		end
	stop_right
			--Stop `Current' d'aller a droite
		do
			going_right:= false
		ensure
			valid_not_going_left: not going_right
		end
	stop_up
			--Stop `Current' de monter
		do
			going_up:= false
		ensure
			valid_not_going_left: not going_up
		end
	stop_down
			--Stop `Current' de decendre
		do
			going_down:= false
		ensure
			valid_not_going_left: not going_down
		end



	inverse_mouvement
			--Inverse le deplacement de `Current' selon sont ancien deplacement
		do
			if going_right then
				stop_right
				going_left:=true
			elseif going_left then
				stop_left
				going_right:=true
			elseif going_up then
				stop_up
				going_down:=true
			elseif going_down then
				stop_down
				going_up:=true
			end
		end


	set_cycle_to_move(a_cycle_to_move:INTEGER_32)
			--Nobre de tour a attendre avant de conter un d�placement de `Current'
		require
			Cycle_Positive: a_cycle_to_move >= 0
		do
			cycle_to_move_current:=0
			cycle_to_move:=a_cycle_to_move
		ensure
			A_ete_asigner :cycle_to_move = a_cycle_to_move
		end

feature

	cycle_to_move_current:INTEGER_32
			--Est le nobre actuele de tour fait par la boucle selont `Current'

	cycle_to_move:INTEGER_32
			--Le nobre de toure avant de fair un d�placment de `Current'

	vecteur_de_vitesse:INTEGER_32
			--Permet de modifier le vitesse de `Current'

--	last_direction:INTEGER_32
--			--Retien la dernire direction que `Current' a eu




feature
note
	copyright: "Copyright (c) 2020, Jonathan Parent"
	license: "[
		GNU General Public License
		Ce programme est libre, vous pouvez le
		redistribuer et/ou le modifier selon les termes
		de la Licence Publique G�n�rale GNU publi�e par
		la Free Software Foundation (version 3 ou bien
		toute autre version ult�rieure choisie par vous).
		Ce programme est distribu� car potentiellement
		utile, mais SANS AUCUNE GARANTIE, ni explicite ni
		implicite, y compris les garanties de
		commercialisation ou d'adaptation dans un but
		sp�cifique. Reportez-vous � la Licence Publique
		G�n�rale GNU pour plus de d�tails.
		Vous devez avoir re�u une copie de la Licence
		Publique G�n�rale GNU en m�me temps que ce
		programme ; si ce n'est pas le cas, visitez:
		<https://www.gnu.org/licenses/>.
		]"

end



