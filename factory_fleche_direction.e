note
	description: "{FACTORY_FLECHE_DIRECTION}Est une factory retorne des image de fleche ayant une direction pour indiquer le sense dont le meuble est placer ."
	author: "Jonathan Parent"
	date: "2020-05-19"


class
	FACTORY_FLECHE_DIRECTION

create
	make


feature {NONE} --Initiation


	make(a_renderer:GAME_RENDERER)
		do
			render := a_renderer

		end



feature-- acces

	fleche_right(a_x,a_y:INTEGER_32):OBJECT_INERTE
			--Cree une fleche qui pointe la droite
		do
			create Result.make(render,"fleche_doite.png")
			Result.set_x (a_x)
			Result.set_y (a_y)

		end


	fleche_left(a_x,a_y:INTEGER_32):OBJECT_INERTE
			--Cree une fleche qui pointe la gauche
		do
			create Result.make(render,"fleche_gauche.png")
			Result.set_x (a_x)
			Result.set_y (a_y)
		end
	fleche_up(a_x,a_y:INTEGER_32):OBJECT_INERTE
			--Cree une fleche qui pointe le haut
		do
			create Result.make(render,"fleche_haut.png")
			Result.set_x (a_x)
			Result.set_y (a_y)
		end

	fleche_down(a_x,a_y:INTEGER_32):OBJECT_INERTE
			--Cree une fleche qui pointe le bas
		do
			create Result.make(render,"fleche_bas.png")
			Result.set_x (a_x)
			Result.set_y (a_y)
		end

	fleche_all_side(a_x,a_y:INTEGER_32):OBJECT_INERTE
			--Cree une fleche qui pointe sur tous les coter
		do
			create Result.make(render,"fleche_4_coter.png")
			Result.set_x (a_x)
			Result.set_y (a_y)
		end

feature --Atribut

	render:GAME_RENDERER
		--Rendu sur le quelle l'image sera afficher






end
