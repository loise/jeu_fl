note
	description: "classe pour sauvegarde les score et charger les setting selon les ficher "
	author: "Jonathan Parent"
	date: "2020-06-05"


class
	FICHIER



create
    make

feature  -- Initialization

    make
            -- Cree un object fichier
        do
        	setting:=""
            create input_file.make_open_read ("save.txt")
            create output_file.make_open_read ("setting.txt")
            create setting_list.make
            create save_list.make
            save_string:= ""
            ip:=""
            port:=12345
            player_name:= "player"
            if output_file.readable then
	            from
	                output_file.read_character
	            until
	                output_file.exhausted
	            loop
	                setting.append_character (output_file.last_character)
	                output_file.read_character
	            end
            end
			set_setting
            input_file.close
            output_file.close
        end


feature	--Fonctional


	read_file(a_file:PLAIN_TEXT_FILE):STRING_32
			--Sert a renvoyer la string de ce qu'il a dans un fichiuer
		local
			l_result:STRING_32
		do
			l_result := ""
			if attached a_file then
				if a_file.readable then
					from
						a_file.read_character
					until
						a_file.exhausted
					loop
						l_result.append_character (a_file.last_character)
						a_file.read_character
					end
				end
			end

			Result:=l_result
		end


	save(a_list,a_comparer:LINKED_LIST[TUPLE[info,donner:STRING_32]])
			--Sert a sauvegarder les score avec le nom du joueur
		local
			i:INTEGER_32
			l_ajouter_new_score:BOOLEAN
		do
			--input_file.open_write
			--input_file.close
			l_ajouter_new_score:=false
			from
				i:=0
				a_list.start
			until
				a_list.exhausted or i=10
			loop
				print("}}}}}}}}}}}}}}}}}}}}}}}}")
				if (a_comparer.item.donner.to_integer_32 > a_list.item.donner.to_integer_32) and not l_ajouter_new_score then
					save_info (a_comparer.item.info,input_file)
					print("}}}}}}}}}}}}}}}}}}}}}}}}")
					save_donner (a_comparer.item.donner, input_file)
					l_ajouter_new_score:=true
				else
					save_info (a_list.item.info, input_file)
					save_donner (a_list.item.donner, output_file)
				end
				save_end_of_line(output_file)
				a_list.forth
				print("comiqwue")
				i:= i+1
			end
		end

	save_donner(a_text:STRING_32;a_file:PLAIN_TEXT_FILE)
			--Sert a save dans un fichier la donner recut
		do
			a_file.open_append
			a_file.put_character ('<')
			a_file.put_string (a_text)
			a_file.put_character ('>')
			a_file.close

		end

	save_end_of_line(a_file:PLAIN_TEXT_FILE)
			--Sert cree la fin d'une ligne
		do
			a_file.open_append
			a_file.put_character (',')
			a_file.new_line
			a_file.close


		end

	save_info(a_text:STRING_32;a_file:PLAIN_TEXT_FILE)
			--Sert a save dans un fichier la donner info

		do
			print(a_text+"{{{{{{{{{{{{{{{{{{{{{{{")
			a_file.open_append
			a_file.put_character ('(')
			a_file.put_string (a_text)
			a_file.put_character (')')
			a_file.close
		end


	get_string_from_setting(a_text:STRING_32):LINKED_LIST[TUPLE[info,donner:STRING_32]]
			--Sert a serpare la liste `a_text' et retour `l_result'
		local
			i:INTEGER_32
			l_value:STRING_32
			l_trouver:STRING_32
			l_debut:BOOLEAN
			l_info_trouver:BOOLEAN
			l_ligne:INTEGER_32
			l_result:LINKED_LIST[TUPLE[info,donner:STRING_32]]
		do
			l_value:=""
			l_info_trouver:=false
			l_trouver:=""
			l_ligne:=1
			create l_result.make
			from
				i:=1
			until
				i = a_text.count+1
			loop
				if a_text.at (i) = '(' then
					l_debut:=true
				elseif a_text.at (i) = ')' then
					l_debut:=false
				elseif a_text.at (i) = '<' then
					l_info_trouver:=true
				elseif a_text.at (i) = '>' then
					l_info_trouver:=false
				elseif l_debut then	--Enregister quelle sorte d'info il a sur la ligne
					l_trouver.append_character (a_text.at (i))
				elseif l_info_trouver then--Enregister l'info sur la ligne
					l_value.append_character (a_text.at (i))
				elseif a_text.at (i) = ',' then
					l_result.extend (l_trouver.to_string_32,l_value.to_string_32)
					--l_result.at (l_ligne).info := l_trouver
					--l_result.at (l_ligne).donner := l_value
					l_trouver:=""
					l_value:=""
					l_ligne:= l_ligne +1
				end
				i:= i+1
			end
			Result := l_result
		end

	set_setting
			--Sert a placer les donner necesair comme ip et le port
		do
			setting_list:=get_string_from_setting (setting)
			from
				setting_list.start
			until
				setting_list.exhausted
			loop
				if setting_list.item.info.is_equal( "ip") then
					ip := setting_list.item.donner
				elseif setting_list.item.info.is_equal("port") then
					port := setting_list.item.donner.to_integer_32
				elseif setting_list.item.info.is_equal ("player_name") then
					player_name := setting_list.item.donner
				end
				print("|"+setting_list.item.info.to_string_32+"|")
				print(setting_list.item.donner+"@")
				setting_list.forth
			end
		end

	get_save_list:LINKED_LIST[TUPLE[info,donner:STRING_32]]
			--Aller cherhcer la liste de save
		do
			Result:=get_string_from_setting(read_file (input_file))
		end




feature -- Access

    input_file: PLAIN_TEXT_FILE
    	--Ficher dans le quelle les donner seron sauvegarder

    output_file: PLAIN_TEXT_FILE
    	--Ficher dans le quelle on charge les donner

    setting:STRING_32
    	--String avec les setting pour le jeu

    setting_list:LINKED_LIST[TUPLE[info,donner:STRING_32]]
    	--Liste de string des info des setting


    save_string:STRING_32
    	--String du fichier de save

    ip:STRING_32
		--Ip pour la comunication
    port:INTEGER_32
    	--Port avec pour la comunication	

    player_name:STRING_32
    	--Nom du joueur qui jou


feature {NONE}

	save_list:LINKED_LIST[TUPLE[info,donner:STRING]]
	    	--Liste des nom et nobre de point de reveil restan
end
